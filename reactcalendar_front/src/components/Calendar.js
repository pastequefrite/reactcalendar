import React from 'react';

import {Row} from 'shards-react';
import {css, StyleSheet} from 'aphrodite'

import moment from 'moment';
import 'moment/locale/fr';

import DatesPanel from './DatesPanel';
import Header from './Header';


class Calendar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentDate: moment(),
            baseDate: moment(),
        };
    }

    _onClick = (next) => {
        if (next) {
            this.setState({currentDate: this.state.currentDate.add(1, 'months')})
        } else {
            this.setState({currentDate: this.state.currentDate.subtract(1, 'months')})
        }
    };

    _getFirstDay = () => {
        return moment(this.state.currentDate).startOf("months").weekday();
    };

    _getLastMonthDays = () => {
        return parseInt(moment(this.state.currentDate).subtract("1", "months").endOf("months").format("D"));
    };

    _getDays = () => {
        let daysLastMonth = [];
        for (let k = this._getFirstDay() - 1; k >= 0; k--) {
            daysLastMonth.push({
                id: k,
                currentMonth: false,
                value: this._getLastMonthDays() - k,
            });
        }

        let daysInMonth = [];
        for (let k = 1; k <= this.state.currentDate.daysInMonth(); k++) {
            let date = k;
            if (k < 10) {
                date = "0" + k;
            }
            daysInMonth.push({
                id: k + this._getFirstDay() - 1,
                currentMonth: true,
                value: k,
                date: this.state.currentDate.format("YYYY-MM-" + date),
            });
        }

        let items = [...daysLastMonth, ...daysInMonth];

        if (items.length < 43) {
            let toIterate = 43 - items.length;
            for (let k = 1; k < toIterate; k++) {
                items.push({
                    id: items.length,
                    currentMonth: false,
                    value: k,
                })
            }
        }

        items.forEach((item, k) => {
            if (moment(this.state.baseDate.format("YYYY-MM-DD")).isSame(item.date)) {
                items[k] = {...item, active: true}
            }
        });
        return items;
    };

    render() {
        return (
            <div>
                <Row>
                    <Header onClick={this._onClick} date={this.state.currentDate}/>
                </Row>
                <Row className={css(styles.container)}>
                    <DatesPanel weekDays={moment.weekdaysShort(true)} days={this._getDays()}/>
                </Row>
            </div>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#2980b9',
    },
});

export default Calendar;
