import React from 'react';

import {Col, Container, Row} from 'shards-react';
import {StyleSheet, css} from 'aphrodite';

import Calendar from './components/Calendar';


function App() {
    return (
        <Container>
            <Row>
                <Col>
                    <h1 className={css(styles.title)}>Calendrier React !</h1>
                </Col>
            </Row>
            <Row>
                <Col className={css(styles.calendarContainer)}>
                    <Calendar/>
                </Col>
            </Row>
        </Container>
    );
}

const styles = StyleSheet.create({
    title: {
        color: '#2980b9',
        textAlign: 'center',
        fontSize: '4em',
    },

    calendarContainer: {
        display: 'flex',
        justifyContent: 'center',
    },
});

export default App;
